﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HeaderFooter.Master" AutoEventWireup="true" CodeBehind="NoiThat.aspx.cs" Inherits="BTL.NoiThat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="style/NoiThat.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="watch_content">
        <%--============================ left menu button========================= --%>
        <div class="side_left_menu">
            <div class="side_left_menu_title">
                Lựa chọn
            </div>
            <div class="side_left_menu_content">
                <div class="side_left_menu_wrap-content">
                    <div class="side_left_menu_list">
                        <div>
                            <p>THƯƠNG HIỆU</p>
                        </div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất TNT</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất DAT</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất VMD</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất TDT</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất USA</span></div><br/></div>
                       

                    </div>
                    <div class=" side_left_menu_list">
                        <div>
                            <p>Phân loại</p>
                        </div>
                        <div class="component_role_watch">
                            <div class=""><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất Gia Đình</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất Quán Bar </span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Nội Thất đôi</span></div>
                        </div>

                    </div>
                    <div class=" side_left_menu_list">
                        <div>
                            <p>KHOẢNG GIÁ</p>
                        </div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Giá < 2 triệu</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Giá từ 2 - 5 triệu</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Giá từ 5 - 10 triệu</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Giá từ 10 - 20 triệu</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Giá > 20 triệu</span></div><br/></div>

                    </div>
                    <div class=" side_left_menu_list">
                        <div>
                            <p>Chất liệu</p>
                        </div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Gỗ</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Bọc da</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Inox</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Cao Su</span></div><br/></div>
                        <div class="component_role_watch">
                            <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                            <div><span>Kiểu Sofa</span></div><br/></div>

                    </div>
                </div>
            </div>
        </div>
        <%-- =======end menu button===================== --%>
         <asp:Repeater ID="rptContentWatchs" runat="server">
                <HeaderTemplate>
                    <div class="wrap-list_watch_content">
                        <div class="navigation_content_watch">
                            <a href="/"><span>Trang chủ</span></a>
                            <b>/</b>
                            <a href="#"><span>Nội Thất</span></a>
                        </div>
                        <div class="wrap_menu_top_of_content">
                            <div class="item_menu_top_content">
                                <div class="checkbox"><input type="checkbox" name="" value="" /></div>
                                <div class=""><span>Giá dưới 2 triệu</span></div><br/>
                            </div>
                            <div class="item_menu_top_content">
                                <div class=""><input type="checkbox" name="" value="" /></div>
                                <div class=""><span>Giá từ 2 - 5 triệu</span></div><br/>
                            </div>
                            <div class="item_menu_top_content">
                                <div class=""><input type="checkbox" name="" value="" /></div>
                                <div class=""><span>Giá từ 5 - 10 triệu</span></div><br/>
                            </div>
                            <div class="item_menu_top_content">
                                <div class=""><input type="checkbox" name="" value="" /></div>
                                <div class=""><span>Giá từ 10 - 20 triệu</span></div><br/>
                            </div>
                            <div class="item_menu_top_content">
                                <div class=""><input type="checkbox" name="" value="" /></div>
                                <div class=""><span>Giá trên 20 triệu</span></div><br/>
                            </div>
                        </div>
                        <div class="container_content_watchs">  
                    </HeaderTemplate>
                    <ItemTemplate>
                         <div class="wrap_item_watch">
                            <a href='/ChiTietNT.aspx?NameDHid=<%#:Eval("chiTietNTid") %>'><img width="55%" src='./images/<%#:Eval("urlPicture") %>' /></a><br/>
                            <a href='/ChiTietNT.aspx?NameDHid=<%#:Eval("chiTietNTid") %>' class="nameProductNoiThat">
                                <span><%#:Eval("nameTypeDH") %><br><%#:Eval("code") %></span>
                            </a>
                            <input type="button" value='<%#:string.Format("{0:N0}", Eval("price")) %> đ' />
                        </div>
                    </ItemTemplate>
                <FooterTemplate>
                     </div>
                    <div class="btn-Add">
                        <a href="#"><input type="button" value="Xem thêm +" /></a>
                    </div>
                </div>
                </FooterTemplate>
            </asp:Repeater>      









    </div>
</asp:Content>
